const { src, dest, watch } = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const concat = require('gulp-concat');
const imagemin = require('gulp-imagemin');

function styles() {
   return src('app/sass/style.sass')
      .pipe(sass({outputStyle: 'compressed'}))
      .pipe(concat('style.min.css'))
      .pipe(dest('app/css'))
}

function images() {
   return src('app/images/**/*')
      .pipe(imagemin(
         [
            imagemin.gifsicle({interlaced: true}),
            imagemin.mozjpeg({quality: 75, progressive: true}),
            imagemin.optipng({optimizationLevel: 5}),
            imagemin.svgo({
               plugins: [
                  {removeViewBox: true},
                  {cleanupIDs: false}
               ]
            })
         ]
      ))
      .pipe(dest('dist/images'))
}

function style_media() {
   return src('app/sass/media.sass')
      .pipe(sass({outputStyle: 'compressed'}))
      .pipe(concat('media.min.css'))
      .pipe(dest('app/css'))
}

function watching() {
   watch(['app/sass/**/*.sass'], styles);
   watch(['app/sass/**/*.sass'], style_media);
}

function build() {
   return src ([
      'app/css/style.min.css',
      'app/css/media.min.css',
      'app/*.html',
    
   ], {base: 'app'})
   .pipe(dest('dist'))
}

exports.watching = watching;
exports.images = images;
exports.build = build;